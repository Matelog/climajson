/*
	Autores: Guillen, Guillermo
			 Corfield, Lautaro
*/
var vec=[];
function DatosNow(){
	function Mostrar(vec){
	return vec.weather;
}
	$.ajax({
		url : "https://api.openweathermap.org/data/2.5/forecast/daily?q=Buenos%20Aires&mode=json&units=metric&cnt=7&APPID=f44ce8aa2623eeb307f7d5fb56faa6a3",
		type : "GET",
		dataType : 'Jsonp',
		success: function(vec){
			var horario = new Date();
			var dias = ["Domingo","Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado","Domingo","Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"];
			var widget = Mostrar(vec);
			console.log(vec.list);
			var x = 0;
			var m = 0;
			var sm = 0;
			var pro = 0;
			var sum = 0;
			//¿CUANTOS DIAS LLOVERÁ?
			for (var z=0; z<7; z++) {
				if(vec.list[z].weather[0].main=='Rain'){				
					x++;
				}
				//a partir de aca se empieza a buscar los dos dias mas ventosos
				for (var i = 0; i <140; i++) {
					if(vec.list[z].speed>i){
						m = vec.list[z].speed;
					}
				}
			}
			//necesito que m sea completamente comparado con todos los valores
			//del array ya que si meto el if de abajo dentro del for de arriba
			//estaria comparando algo que todavia no está definido ya que
			//está buscando el speed mas alto para despues compararlo abajo
			for (var z=0; z<7; z++) {
				for(var i = 0; i <140; i++){
					if(vec.list[z].speed<m&&vec.list[z].speed>i){
						sm = vec.list[z].speed;
					}
				}
				//acá empieza el promedio de temperatura de todos los dias
				pro = vec.list[z].temp.day;
				sum +=pro; 
			}
			console.log("Los dias en los cuales lloverá son: "+x);
			console.log("Día más ventoso: "+m+" km/h");
			console.log("Día segundo más ventoso: "+sm+" km/h");
			console.log("El promedio de temperatura de la semana es: "+(sum / z).toString().substr(0,5));
			//
			/*¿CUALES SON LOS DIAS MAS VENTOSOS?
			1 Aire ligero 1-5 
			2 Brisa ligera 6-11 
			3 Brisa suave 12-19 
			4 Brisa moderada 20-28 
			5 Brisa fresca 29-38 
			6 Brisa fuerte 39-49 
			7 Viento moderado 50-61 
			8 Viento fresco 62-74 
			9 Viento fuerte 75-88 
			10 Viento fortísimo 89-102 
			11 Tempestad 103-117 
			12 Huracán 118-133 
			*/

			for(var j = 0; j<24;j++){	
				if(j>=12&&j<=20){
					switch(horario.getHours()){
						case j:
							for (var i = 0; i < 7; i++) {
								//detecta el dia
								$("#fec").html(horario.getDate() + "/" + (horario.getMonth() +1) + "/" + horario.getFullYear());//Le sumo 1 al mes porque empieza desde 0
								$("#"+"fecha_"+i).html(dias[horario.getUTCDay()+i]);
								$("#temp_actual_"+i).html(vec.list[i].temp.eve.toString());
								$("#temp_max_"+i).html(vec.list[i].temp.max.toString().substr(0,vec.list[i].temp.max.toString().length-1));
								//console.log("dia_"+i+" " +vec[0].daily.data[i].icon);
								$("#dia_"+i).attr('src', "../weather array/images/icons/"+vec.list[i].weather[0].icon+".svg");				
							}
						break	
					}
				}
				if(j<=12&&j>=6){
					switch(horario.getHours()){
						case j:
							for (var i = 0; i < 7; i++) {
								$("#fec").html(horario.getDate() + "/" + (horario.getMonth() +1) + "/" + horario.getFullYear());
								$("#"+"fecha_"+i).html(dias[horario.getUTCDay()+i]);
								$("#temp_actual_"+i).html(vec.list[i].temp.morn.toString());
								$("#temp_max_"+i).html(vec.list[i].temp.max.toString().substr(0,vec.list[i].temp.max.toString().length-1));
								//console.log("dia_"+i+" " +vec[0].daily.data[i].icon);
								$("#dia_"+i).attr('src', "../weather array/images/icons/"+vec.list[i].weather[0].icon+".svg");				
							}
						break	
					}
				}
				if(j>=20){
					switch(horario.getHours()){
						case j:
							for (var i = 0; i < 7; i++) {
								$("#fec").html(horario.getDate() + "/" + (horario.getMonth() +1) + "/" + horario.getFullYear());
								$("#"+"fecha_"+i).html(dias[horario.getUTCDay()+i]);
								$("#temp_actual_"+i).html(vec.list[i].temp.night.toString());
								$("#temp_max_"+i).html(vec.list[i].temp.max.toString().substr(0,vec.list[i].temp.max.toString().length-1));
								//console.log("dia_"+i+" " +vec[0].daily.data[i].icon);
								$("#dia_"+i).attr('src', "../weather array/images/icons/"+vec.list[i].weather[0].icon+".svg");				
							}
						break	
					}
				}
				if(j<=6){
					switch(horario.getHours()){
						case j:
							for (var i = 0; i < 7; i++) {
								$("#fec").html(horario.getDate() + "/" + (horario.getMonth() +1) + "/" + horario.getFullYear());
								$("#"+"fecha_"+i).html(dias[horario.getUTCDay()+i]);
								$("#temp_actual_"+i).html(vec.list[i].temp.night.toString());
								$("#temp_max_"+i).html(vec.list[i].temp.max.toString().substr(0,vec.list[i].temp.max.toString().length-1));
								//console.log("dia_"+i+" " +vec[0].daily.data[i].icon);
								$("#dia_"+i).attr('src', "../weather array/images/icons/"+vec.list[i].weather[0].icon+".svg");				
							}
						break	
					}
				}

			}
		}
	});

}

